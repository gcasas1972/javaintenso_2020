package dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import model.Empleado;
import utils.JpaUtils;

public class DaoEmpleado {

	public static void create(Empleado persona) {
		EntityManager em=JpaUtils.getEntityManagerFactory().createEntityManager();
		em.getTransaction().begin();
		
		try {
			em.persist(persona);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			System.out.println("ocurrio un error al guardar al empleado");
			e.printStackTrace();
		}finally {
			em.close();
		}
		
		
	}
	
	public static void update(Empleado persona) {
		EntityManager em=JpaUtils.getEntityManagerFactory().createEntityManager();
		EntityTransaction tx=em.getTransaction();
		tx.begin();
		
		try {
			em.merge(persona);
			tx.commit();
		} catch (Exception e) {
			tx.rollback();
			System.out.println("ocurrio un error al actualizar al empleado");
			e.printStackTrace();
		}finally {
			em.close();
		}
		
	}
	
	public static Empleado find(Long id) {
		EntityManager em=JpaUtils.getEntityManagerFactory().createEntityManager();
		Empleado p=null;
		
		try {
			p=em.find(Empleado.class, id);
		} catch (Exception e) {
			System.out.println("no encontro al empleado");
			e.printStackTrace();
		}finally {
			em.close();
		}	
		
		return p;
	}
	
	public static void delete(Long id) {
		EntityManager em=JpaUtils.getEntityManagerFactory().createEntityManager();
		em.getTransaction().begin();
		
		try {
			Empleado p=DaoEmpleado.find(id);
			em.remove(p);
			em.getTransaction().commit();
			
		} catch (Exception e) {
			em.getTransaction().rollback();
			System.out.println("no borro al empleado");
			e.printStackTrace();
		}finally {
			em.close();
		}
	}
}
