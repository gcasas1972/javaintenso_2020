package dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import model.Persona;
import utils.JpaUtils;

public class DaoPersona {

	public static void create(Persona persona) {
		EntityManager em=JpaUtils.getEntityManagerFactory().createEntityManager();
		em.getTransaction().begin();
		
		try {
			em.persist(persona);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			System.out.println("ocurrio un error al guardar la persona");
			e.printStackTrace();
		}finally {
			em.close();
		}
		
		
	}
	
	public static void update(Persona persona) {
		EntityManager em=JpaUtils.getEntityManagerFactory().createEntityManager();
		EntityTransaction tx=em.getTransaction();
		tx.begin();
		
		try {
			em.merge(persona);
			tx.commit();
		} catch (Exception e) {
			tx.rollback();
			System.out.println("ocurrio un error al actualizar la persona");
			e.printStackTrace();
		}finally {
			em.close();
		}
		
	}
	
	public static Persona find(Long id) {
		EntityManager em=JpaUtils.getEntityManagerFactory().createEntityManager();
		Persona p=null;
		
		try {
			p=em.find(Persona.class, id);
		} catch (Exception e) {
			System.out.println("no encontro la persona");
			e.printStackTrace();
		}finally {
			em.close();
		}	
		
		return p;
	}
	
	public static void delete(Long id) {
		EntityManager em=JpaUtils.getEntityManagerFactory().createEntityManager();
		em.getTransaction().begin();
		
		try {
			Persona p=DaoPersona.find(id);
			em.remove(p);
			em.getTransaction().commit();
			
		} catch (Exception e) {
			em.getTransaction().rollback();
			System.out.println("no borro a la persona");
			e.printStackTrace();
		}finally {
			em.close();
		}
	}
}
