package ejecutable;



import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import dao.DaoCiudad;
import dao.DaoEmpleado;
import dao.DaoPais;
import dao.DaoPersona;
import model.Ciudad;
import model.Empleado;
import model.Pais;
import model.Persona;

public class Inicio {

	public static void main(String[] args) {
	//	testPersona();
	//	crearPaises();
	//	crearCiudades();
	//	testEmpleado();
		testPaisCiudad();
	}
	
	public static void testPersona() {
		Persona p = new Persona();
		Date d=new Date("2022/07/13"); // en la BD lo guarda asi: 2022-07-13
		p.setNombre("Carmen");
		p.setApellido("Barbieri");
		p.setCedula("JHK333");
		p.setFechaNacimiento(d);
		DaoPersona.create(p);
			
		System.out.println("--- Persona en BD ----");
		System.out.println(p.getNombre() + ", " + p.getApellido());
		Persona p1=DaoPersona.find(1L);
		System.out.println(p1.getNombre() + ", " + p1.getApellido());
		
	}

	public static void testEmpleado() {
		Empleado e=DaoEmpleado.find(1L);
		System.out.println("--- EL EMPLEADO DEL MES -----");
		System.out.println(e.getUsuario());
		System.out.println(e.getPerson().getNombre());
	}
	public static void crearPaises() {
		Pais p=new Pais();
		p.setNombre("Espa�a");
		DaoPais.create(p);
		p=new Pais();
		p.setNombre("Argentina");
		DaoPais.create(p);
	}
	
	public static void crearCiudades() {
//		// ESPA�A
//		Pais p=DaoPais.find(1L);
//		Ciudad c=new Ciudad();
//		c.setNombre("Madrid");
//		c.setPais(p);
//		DaoCiudad.create(c);
//	//	DaoCiudad.update(c);
//		
//		Ciudad c1=new Ciudad();
//		c1.setNombre("Barcelona");
//		c1.setPais(p);
//		DaoCiudad.create(c1);
//	//	DaoCiudad.update(c1);
//		
//		Ciudad c2=new Ciudad();
//		c2.setNombre("Valencia");
//		c2.setPais(p);
//		DaoCiudad.create(c2);
//	//	DaoCiudad.update(c2);
//		
//		Set<Ciudad> ciudades=new HashSet<Ciudad>();
//		ciudades.add(c);
//		ciudades.add(c1);
//		ciudades.add(c2);
//				
//		System.out.println("--- EL PAIS DE -----");
//		System.out.println(p.getNombre());
//		
//		p.setCiudades(ciudades);
//		
//		
		// ARGENTINA
		Pais p2=DaoPais.find(2L);
		Ciudad c3=new Ciudad();
		c3.setNombre("Buenos Aires");
		c3.setPais(p2);
		DaoCiudad.create(c3);
		
		Ciudad c4=new Ciudad();
		c4.setNombre("Rosario");
		c4.setPais(p2);
		DaoCiudad.create(c4);
		
		Ciudad c5=new Ciudad();
		c5.setNombre("Campana");
		c5.setPais(p2);
		DaoCiudad.create(c5);
		
		Set<Ciudad> ciudades1=new HashSet<Ciudad>();
		ciudades1.add(c3);
		ciudades1.add(c4);
		ciudades1.add(c5);
		
		p2.setCiudades(ciudades1);	
		}
	
	public static void testPaisCiudad() {
		
		Pais p=DaoPais.find(1L);
		System.out.println("--- EL PAIS DE -----");
		System.out.println(p.getNombre());
		
		List<Ciudad> ciudades=DaoCiudad.findAllCiudades(1);
		for(int i=0; i<ciudades.size();i++) {
			System.out.println(ciudades.get(i));
		}
		
	}
	
}
