package clase1;

public class Circulo {
// declaracion de Atributos 
// sinonimos : propiedades, caracteristicas, campos, variables, descripciones
	
	String color="negro"; // visibilidad de package
private	String color1;
public String color2; // inicia en null, porque String es un objeto
protected String color3 = "blanco"; //atributo inicializado

// declararion de Metodos
// sinonimos: procedure, eventos, funciones, rutinas, subrutinas, servicios, acciones, comportamiento

public void imprimirColor(String color) { // void --> que es un rutina, no devuelve NADA
	System.out.println("el color es " + color); // apunta al parametro
	System.out.println("el color es " + this.color); // apunta al atributo

	String nombre=new String();
	nombre="Ale";
	
	String apellido="Gomez";

}

public double calcularPerimetro(int diametro ) { // return --> que es una funcion, devuelve ALGO
	return diametro * Math.PI;
}

public static double calcularArea(double radio) {
	return Math.PI * radio * radio;
}
	
}
