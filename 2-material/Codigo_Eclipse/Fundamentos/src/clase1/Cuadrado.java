package clase1;

// es un POJO --> plain old java object, value object, data transfer object
public class Cuadrado {
	// ENCAPSULAMIENTO DE LA INFORMACION
	// atributos con VISIBILIDAD PRIVATE
private String color="amarillo";
private double lado;

public String getColor() {
	return color;
}
public void setColor(String color) {
	this.color = color;
}
public double getLado() {
	return lado;
}
public void setLado(double lado) {
	this.lado = lado;
}

// METODOS ACCESORES con VISIBILIDAD PUBLICA



}
