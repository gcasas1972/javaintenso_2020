package clase1;

public class TestCirculo {

	public static void main(String[] args) {
		//operoCirculos();
		//pruebaNros();
		operoCuadrados();
	}

	//metodos de clase
	private static void operoCirculos() {
		// crear una instancia de Circulo con NEW
				Circulo cir = new Circulo();
				Circulo cir2 = new Circulo();
				// tipo de clase - nombre de instancia - llamado al metodo constructor

				System.out.println("el colorNro3 es " + cir.color3);

				System.out.println("llamo a imprimir ");
				cir.imprimirColor("rojo"); // llamado de instancia

				// recibir en una variable local de main, la rpta del retorno
				double diametro = cir.calcularPerimetro(56);
				System.out.println("el perimetro para el circulo es " + diametro);

				// llamado directo, llamado a STATIC, llamado de Clase
				System.out.print("el area es ");
				System.out.println(Math.round(Circulo.calcularArea(22.3)));
	}
	
	private static void pruebaNros() {
		System.out.println("estoy probando nros");
		// tipos primitivos
		// sin DECIMALES         con Decimales
		// byte   (8 bits)      boolean (1 bit)
		// short  (16 bits)     char  --> maneja carecters
		// int    (32 bits)     float
		// long   (64 bits)     double
		
		//2.1 �es un double o un float? --> java lo ve como DOUBLE
		float c = 2.1F;
		float c1 = 2.1f;
		double d1= 2.1;
		
		// 8 � es un int o es un long? --> java lo ve como INTEGER
		
		int a= 8;
		long b=8L;
		
		boolean bandera=true; // nunca se usa el 0 o el 1
		
		char valor ='A';
		
		int x=1;
		x++; // x=x+1
		System.out.println("el valor de X es " + x); // 2
		
		int i=1;
		int j=1;
		j=i++; 
		System.out.println("el valor de J es " + j); // 1
		
		i=1;
		j=1;
		j=++i;
		System.out.println("el valor de J es " + j); // 2
		
	}

	private static void operoCuadrados() {
		Cuadrado c=new Cuadrado();
		System.out.println(c.getColor());

		c.setColor("verde");
		System.out.println(c.getColor());
	}
}
