package clase2;

public class TestRepeticiones {

	public static void main(String[] args) {
		TestRepeticiones test=new TestRepeticiones();
		//test.usoFor();
		//test.usoWhile();
		//test.usoBreakContinue();
		test.usoBreakContinue2();

	}
	void usoFor() {
	// SE USA CUANDO SE CONOCE EL TOPE DE REPETICIONES O ITERACIONES
		// 1er ejemplo
		// i = iteracion
		for(int i=1;i<=10;i++) { // i++ --> i=i+1
			System.out.println("el valor de i es " + i);
		}
		// 2do ejemplo
		int j=0;
		for(;j<10;j++) {
			System.out.println("el valor de j es " + j);
		}
		System.out.println("--------------");
		// 3er ejemplo
		for(int i=10; i>=0; i--) {
			System.out.println("el valor de i para atras es " + i);
		}
		System.out.println("--------------");
		// 4to ejemplo
		for(int i=0; i<10; i+=2) { // i=i+2
			System.out.println("el valor de i par es " +i);
		}
	}

	void usoWhile() {
		int cont=10;
		// SE USA CUANDO NO SE CONOCE EL TOPE DE REPETICIONES
		// USO WHILE, cuando quiero ejecutar entre 0 y N veces
		while(cont<10) {
			System.out.println("el contador Nro1 " + cont);
			cont++;
		}
		System.out.println("Sali del While");
		
		// USO DO WHILE, cuando quiero ejecutar entre 1 y N veces
		// me aseguro de EJECUTAR AL MENOS UNA VEZ, CUMPLA O NO LA CONDICION
		int cont1=10;
		do {
			System.out.println("el contador Nro2 " + cont1);
			cont1++;
		}while(cont1<10);
		
	}

	void usoBreakContinue() {
		// no imprime: el 2, 3, y 4
		for(int i=0; i<=4;i++) {
			//if(i==2)break;
			if(i==2) {
				break; // se va del for, no ejecuta 4 veces
			}
			System.out.println("Iteracion nro " + i);
		}
		System.out.println("-----------------");
		// no imprime el 2
		for(int i=0; i<=4;i++) {
			if(i==2)continue;
			System.out.println("Iteracion nro " + i);
		}
	}

	void usoBreakContinue2() {
		int i=0;
		while(true) {
			i++;
			int j=i*27;
			if (j==1269) {
				break;
			}
			if (i % 10 !=0)continue;
			System.out.println(i);
		}
	}
}
