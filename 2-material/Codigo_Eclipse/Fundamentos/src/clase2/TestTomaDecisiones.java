package clase2;

public class TestTomaDecisiones {

	public static void main(String[] args) {
		TestTomaDecisiones test1 = new TestTomaDecisiones();
		// test1.verCantAsistentes();
//		System.out.println(test1.verAsistentes());
//		// otra forma de ver el retorno de unm metodo
//		String salida=test1.verAsistentes();
//		System.out.println(salida);
//		test1.usoSwitch();
		test1.usoSwitch2();
	}

	// metodo de instancia
	public void verCantAsistentes() {
		System.out.println("estoy en el metodo verCantAsistentes");
		// EJEMPLO 1
		int asistentes = 2;
		if (asistentes >= 5) {
			System.out.println("hay MAS de 5");// salida por TRUE
		} else {
			System.out.println("hay menos de 5"); // salida por FALSE
		}
		System.out.println("-------------------------");
		// EJEMPLO 2
		// uso la Y logica con &, && , o con O | ... ||
		String clima = "soleado";
		if (asistentes >= 5 && clima == "lluvioso") {
			System.out.println("hay MAS de 5 y " + clima);// salida por TRUE
		} else {
			System.out.println("hay menos de 5 y " + clima); // salida por FALSE
		}
		System.out.println("-------------------------");
		// EJEMPLO 3
		asistentes = 9;
		clima = "lluvioso";
		// anidamiento
		if (asistentes >= 5) {
			if (clima == "lluvioso") {
				System.out.println("hay MAS de 5 y " + clima);// salida por TRUE
			} else {
				System.out.println("hay menos de 5 y " + clima); // salida por FALSE
			}
		}
	}

	public String verAsistentes() {
		int asistentes = 2;
		String rpa = "";
		// IF reducido --> ? = entonces --> : else
		rpa = (asistentes >= 5 ? "SI" : "NO");
		return rpa;
	}

	public void usoSwitch() {
		int dia = 4;

		switch (dia) {
		case 1:
			System.out.println("hoy es lunes");
			break;
		case 2:
			System.out.println("hoy es martes");
			break;
		case 3:
			System.out.println("Mi�rcoles");
			break;
		case 4:
			System.out.println("Hoy es Jueves");
			break;
		case 5:
			System.out.println("Viernes");
			break;
		case 6:
			System.out.println("S�bado");
			break;
		case 7:
			System.out.println("Domingo");
			break;

		default:
			System.out.println("no es un nro de dia valido");
		}

		System.out.println("********************");

	}

	public void usoSwitch2() {
		// tipos aceptados de variables del switch
		// byte, short, int, long, char, boolean, String
		int mes=7;
		switch(mes) {
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			System.out.println("tiene 31 dias");break;
		case 4:
		case 6:
		case 9:
		case 11:
			System.out.println("tiene 30 dias");break;
		case 2:
			System.out.println("tiene 28/29 dias");break;
		default: System.out.println("no es un nro de mes valido");
		}
		 System.out.println("salio del switch");
		 
		// lo mismo pero con IF
		 if(mes==1 || mes ==3 || mes ==5 || mes==7 || mes==8 || mes == 10 || mes ==12) {
			 System.out.println("tiene 31 dias");
		 } else if (mes==4 || mes ==6 || mes ==9 || mes==11 ){
			 System.out.println("tiene 30 dias");
		 } else if (mes == 2) {
			 System.out.println("tiene 28/29 dias");
		 } else {
			 System.out.println("no es un nro de mes valido");
		 }
	}
}
