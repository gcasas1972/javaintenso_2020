package clase3;

public class Auto {
	// POJO plain old java object
	// el auto esta formado por Rueda y Volante
	// el auto es el TODO, rueda y volante son las PARTES
	// es una relacion de composicion
private Rueda rueda;
private Volante volante;

public Rueda getRueda() {
	return rueda;
}
public void setRueda(Rueda rueda) {
	this.rueda = rueda;
}
public Volante getVolante() {
	return volante;
}
public void setVolante(Volante volante) {
	this.volante = volante;
}



}
