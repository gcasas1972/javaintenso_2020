package clase3;

public class Empleado {
String nombre; // visibilidad package ... para los vecinos
protected String apellido; // visibilidad para las clases hijas y los vecinos del package
private double sueldo; // visibilidad para la clase actual

public String getDetalles() { // visibilidad para cualquiera
	return "Nombre: " + nombre + ", gana " + sueldo;
}

public double getSueldo() {
	return sueldo;
}

public void setSueldo(double sueldo) {
	this.sueldo = sueldo;
}
}
