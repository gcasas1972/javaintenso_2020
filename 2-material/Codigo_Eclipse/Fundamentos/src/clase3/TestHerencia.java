package clase3;

import clase3_bis.Secretaria;

public class TestHerencia {

	public static void main(String[] args) {
		Empleado e= new Empleado();
		e.nombre="Ale";
		e.apellido="Buq";
		e.setSueldo(5000000);		
		System.out.println(e.getDetalles());
		
		Gerente g=new Gerente();
		g.nombre="Daniel";
		g.apellido="Orl";
		g.setSueldo(10);
		System.out.println(g.getDetalles());
		
		Secretaria s= new Secretaria();
		s.apellido="Perez";
	}

}
