package clase3_abstract;

public abstract class Mamifero { // prohibido hacer instancias de MAMIFERO

	// LOS HIJOS DEBEN IMPLEMENTAR EL METODO MARCADO CON ABSTRACT @OVERRIDE
public abstract void ba�ar(); // que define y no tiene codigo entre {}
public void hacerSonido() {
	System.out.println("el mamifero hace grrrrr");
}

public void alimentar() {
	System.out.println("el mamifero come");
}
}
