package clase3_abstract;

public class TestAnimal {

	public static void main(String[] args) {
	//	Mamifero m= new Mamifero(); // mamifero es abstract

		Canino c= new Canino();
		c.alimentar();
		c.ba�ar();
		
		Felinos f= new Felinos();
		f.alimentar();
		f.ba�ar();
		// otra forma de instanciar, usando POLIMORFISMO
		System.out.println("-------------------------");
		Mamifero m1=new Canino();
//		m1.alimentar();
//		m1.ba�ar();
		Mamifero m2=new Felinos();
//		m2.alimentar();
//		m2.ba�ar();
		verDetalles(m1);
		verDetalles(m2);
	}
	
	public static void verDetalles(Mamifero m) {
		m.alimentar();
		m.ba�ar();
		m.hacerSonido();
		
		if (m instanceof Canino) {
			Canino c=(Canino)m; // casting de mamifero a canino
			c.mostrar();
		}
		
	}

}
