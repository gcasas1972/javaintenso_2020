package clase3_final;

public class Bermuda extends Pantalon{

//	@Override 
	// no se puede sobreescribir un metodo marcado como FINAL
//	public void verColor() {
//		System.out.println(this.color);
//	}

	@Override
	public void verTalle() {
		System.out.println(super.talle);
		System.out.println("mi talle es XL");
	}
}
