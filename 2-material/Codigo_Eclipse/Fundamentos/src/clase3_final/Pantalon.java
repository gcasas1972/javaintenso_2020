package clase3_final;

public class Pantalon {
String color="negro";
String talle;
double precio;
final String TIPO_TELA="Algodon"; // lo ve como constante

// PROHIBIR EL OVERRIDE
public final void verColor() {
	System.out.println(this.color);
}

public void verTalle() {
	System.out.println(this.talle);
}
}
