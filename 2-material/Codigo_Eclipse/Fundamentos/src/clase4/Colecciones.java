package clase4;

public class Colecciones {
public static void main(String[] args) {
	// argumentos escritos en el Run Configuration del Proyecto
	System.out.println(args[0]);
	System.out.println(args[1]);
	
//	vectores0();
//	vectores1() ;
	matrices();
}
public static void vectores0() {
	String[] saludos=new String[4];	
	saludos[0]="hola";
	saludos[1]="Adios";
	saludos[2]="Hello";
	saludos[3]="Goodbye";
	
	try {
	for (int i = 0; i<=saludos.length;i++) {
		System.out.println(saludos[i]);
	}
	}catch (ArrayIndexOutOfBoundsException e1) {
		System.out.println("ojo pusiste un IGUAL en el for");
		System.out.println(e1.getMessage());
	}
}

public static void vectores1() {
	int[] miVector=new int[3];
	miVector[0]=99;
	miVector[1]=199;
	miVector[2]=299;
	
	int[] miVector2= {3,7,15};
	for (int i = 0; i<miVector2.length;i++) {
		System.out.println(miVector2[i]);
	}
	
	String[] miVector3= {"Martin", "Jose", "Pablo"};
	for(String elemento : miVector3) { // for each
		System.out.println(elemento);
	}
	
}

public static void matrices() {
	// las matrices son como tablas, tenemos filas y columnas
	// debemos usar un for anidado (for -->filas, for -->columnas)
	// 2x3
	String[][] ciudades= {{"BsAs","SaoPablo","Madrid"},
						  {"Argentina","Brasil","Espa�a"}			
						  };
	
	for(int i=0;i<ciudades.length;i++) { // recorro por filas
		for(int j=0;j<ciudades[i].length;j++) { // recorro por columna
			System.out.print(ciudades[i][j] + ", ");
		}
		System.out.println();
	}
	
}
}
