package clase4_Exceptions;

// 2do paso, crear un metodo que ARROJE la exception personalizada
public class Conexion {

	public void conectar(String nombreServer) throws ServerTimeOutException{
		boolean exito;
		int puertoAconectar=80;
		exito=abrir(nombreServer, puertoAconectar);
		if (!exito) {
			throw new ServerTimeOutException("no me puedo conectar", puertoAconectar);
		}
		
	}
	
	public boolean abrir(String nombreServer, int puertoAconectar) {
		System.out.println("estoy abriendo el server " + nombreServer);
		return false;
	}
}
