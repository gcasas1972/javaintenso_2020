package clase4_Exceptions;
// 1er paso, construir una Exception personalizada

public class ServerTimeOutException extends Exception{
private int puerto;

public ServerTimeOutException(String message, int puerto) {
	super(message);
	this.puerto=puerto;
}

public int getPuerto() {
	return puerto;
}


}
