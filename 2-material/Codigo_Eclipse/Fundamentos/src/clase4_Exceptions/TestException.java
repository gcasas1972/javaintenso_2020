package clase4_Exceptions;

public class TestException {

	public static void main(String[] args) {
		String[] saludos=new String[4];	
		saludos[0]="hola";
		saludos[1]="Adios";
		saludos[2]="Hello";
		saludos[3]="Goodbye";
		
		 Conexion c=new Conexion();
		 
		 try {
			 for (int i = 0; i<saludos.length;i++) {
					System.out.println(saludos[i]);
				}
			c.conectar("Capgemini");
		 }catch (ArrayIndexOutOfBoundsException e1) {
				System.out.println("ojo pusiste un IGUAL en el for");
				System.out.println(e1.getMessage());
		} catch (ServerTimeOutException e) {
			e.printStackTrace();
			System.out.println(e.getMessage()+ " al puerto " + e.getPuerto());
		} catch (Exception e2) {
			System.out.println(e2.getMessage());
		} finally {
			System.out.println("fin");
		}

	}

}
