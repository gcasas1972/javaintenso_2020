package clase4_Listas;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ListEjemplo {

	public static void main(String[] args) {
		// tipo interface List.... una instancia de la clase concreta ArrayList
		// lista ordenada con elementos duplicados
		ArrayList lista1=new ArrayList();
		List lista=new ArrayList();
		lista.add("uno");
		lista.add("segundo");
		lista.add("3ero");
		lista.add(4);
		lista.add("segundo");
		lista.add(2.5);
		lista.add(true);
		lista.add(4);
		System.out.println(lista);
		
		Iterator it=lista.iterator();

		while(it.hasNext()) {
			System.out.print(" " + it.next());
		}
	}

}
