package clase5_Collections;

import java.util.Comparator;

public class ComparadorPersona implements Comparator<Persona> {

	@Override
	public int compare(Persona o1, Persona o2) {
		return o1.getNombrePila().compareTo(o2.getNombrePila());
	}

}
