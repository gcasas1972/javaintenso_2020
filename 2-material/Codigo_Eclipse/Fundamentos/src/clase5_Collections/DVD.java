package clase5_Collections;

import java.util.List;

public class DVD implements Comparable<DVD> {
int codigo;
String titulo, director;
List actores;
@Override
public int compareTo(DVD o) {
	final int MENOR= -1;
	final int MAYOR=1;
	final int IGUAL=0;
	if(this.codigo==0) return IGUAL;
	int esteCodigo= new Integer(this.codigo);
	int otroCodigo= new Integer(o.codigo);
	if (esteCodigo < otroCodigo) return MENOR;
	if (esteCodigo > otroCodigo) return MAYOR;
	return IGUAL;
}
public int getCodigo() {
	return codigo;
}

}
