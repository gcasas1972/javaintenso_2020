package clase5_Collections;

import java.util.*;

public class TestCollections {

	public static void main(String[] args) {
		// EJEMPLOS PARA COLECCIONES QUE NO ADMITEN DUPLICADOS
		ejemplo1();
		System.out.println("****************");
		ejemplo2();
		System.out.println("****************");
		ejemplo3();
	}
	public static void ejemplo1() {
        Set<String> ciudades = new HashSet<String>();
		
		ciudades.add("Buenos Aires");
		ciudades.add("Tucum�n");
		ciudades.add("Rosario");
		ciudades.add("Buenos Aires");
		ciudades.add("Bariloche");
		
		
		Iterator<String> it = ciudades.iterator();
		
		while (it.hasNext()) {
			System.out.println("Ciudad:"+ it.next());
		}

	}
	public static void ejemplo2() {
		// LinkedHashSet, respeta el orden de carga a la coleccion
        Set<String> ciudades = new LinkedHashSet<String>();
		
		ciudades.add("Buenos Aires");
		ciudades.add("Tucum�n");
		ciudades.add("Rosario");
		ciudades.add("Buenos Aires");
		ciudades.add("Bariloche");
		
		
		Iterator<String> it = ciudades.iterator();
		
		while (it.hasNext()) {
			System.out.println("Ciudad:"+ it.next());
		}

	}

	public static void ejemplo3() {
		// TreeSet, muestra ordenado alfabeticamente
		Set<String> ciudades = new TreeSet<String>();
		
		ciudades.add("Buenos Aires");
		ciudades.add("Tucum�n");
		ciudades.add("Rosario");
		ciudades.add("Buenos Aires");
		ciudades.add("Bariloche");
		
		
		Iterator<String> it = ciudades.iterator();
		
		while (it.hasNext()) {
			System.out.println("Ciudad:"+ it.next());
		}

	}
}
