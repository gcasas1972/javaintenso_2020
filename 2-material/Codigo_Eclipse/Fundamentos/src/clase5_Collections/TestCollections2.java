package clase5_Collections;

import java.util.*;

public class TestCollections2 {

	public static void main(String[] args) {
		ejemplo1();
		System.out.println("****************");
		ejemplo2();
		System.out.println("****************");
		ejemplo3();

	}

	public static void ejemplo1() {
		List<String> ciudades = new ArrayList<String>();

		ciudades.add("Buenos Aires");
		ciudades.add("Tucum�n");
		ciudades.add("Rosario");
		ciudades.add(1, "Paran�");
		ciudades.add("Buenos Aires");
		ciudades.add("Bariloche");
		
		Iterator<String> it = ciudades.iterator();

		while (it.hasNext()) {
			System.out.println("Ciudad:" + it.next());
		}

	}
	public static void ejemplo2() {
		List<String> ciudades = new LinkedList<String>();

		ciudades.add("Buenos Aires");
		ciudades.add("Tucum�n");
		ciudades.add("Rosario");
		ciudades.add(1, "Paran�");
		ciudades.add("Buenos Aires");
		ciudades.add("Bariloche");

		Iterator<String> it = ciudades.iterator();

		while (it.hasNext()) {
			System.out.println("Ciudad:" + it.next());
		}

	}
public static void ejemplo3() {
	List<String> ciudades = new Vector<String>();

	ciudades.add("Buenos Aires");
	ciudades.add("Tucum�n");
	ciudades.add("Rosario");
	ciudades.add(1, "Paran�");
	ciudades.add("Buenos Aires");
	ciudades.add("Bariloche");
	
	Iterator<String> it = ciudades.iterator();

	while (it.hasNext()) {
		System.out.println("Ciudad:" + it.next());
	}

	}
}
