package clase5_Collections;

import java.util.*;

public class TestCollections3 {

	public static void main(String[] args) {
		// MAP --> define clave/valor 
		ejemplo1();
		System.out.println("****************");
		ejemplo2();
		System.out.println("****************");
		ejemplo3();

	}

	public static void ejemplo1() {
		// Map<String, Persona> la clave es de tipo String, y el valor es Persona
	Persona p1= new Persona("Victor", "Montiveo");
	Persona p2= new Persona("Patricio", "Orce");
	Map<String, Persona> personas=new HashMap<String, Persona>();
	personas.put(p1.getNombrePila(),p1);
	personas.put(p2.getNombrePila(),p2);
	
	Set<String> claves=personas.keySet();
	Iterator<String> it= claves.iterator();
	while (it.hasNext()) {
		String clave =it.next();
		Persona p= personas.get(clave);
		System.out.println(clave + ": " + p.getApellido());
	}
	
	
	}
	public static void ejemplo2() {
		DVD d1= new DVD();
		DVD d2= new DVD();
		DVD d3= new DVD();
		d1.codigo=11;
		d2.codigo=7;
		d3.codigo=11;
		
		System.out.println(d1.compareTo(d3)); // IGUAL
		System.out.println(d1.compareTo(d2)); // MAYOR
		List<DVD> lista = new ArrayList<DVD>();
		lista.add(d1);
		lista.add(d2);
		lista.add(d3);
		for(DVD elemento: lista) {
		System.out.print (elemento.codigo + ", ");
		}
		System.out.println("****************");
		Collections.sort(lista);		
		for(DVD elemento: lista) {
			System.out.print (elemento.codigo + ", ");
			}
		
	}
	public static void ejemplo3() {
		
	}
}
