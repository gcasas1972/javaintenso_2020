package clase5_fechas;

//import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class TestFechas {

	public static void main(String[] args) {
		//ejemplo1();
		ejemplo2();
	}

	public static void ejemplo1() {
		Date miFecha = new Date();
		System.out.println(miFecha);
		Date miFecha2 = new Date(22,6,2022);
		boolean rpta=miFecha.after(miFecha2);
		System.out.println(rpta);
	}
	public static void ejemplo2() {
		Date miFecha = new Date();
		Calendar cal= Calendar.getInstance();
		System.out.println(cal.getTime().getHours());
		
		GregorianCalendar ahora=new GregorianCalendar();
		System.out.println(ahora.getTime().getHours());
		
		int mes= ahora.get(Calendar.MONTH);
		int diaSem= ahora.get(Calendar.DAY_OF_WEEK);
		System.out.println("mes: " + mes + " dia: " + diaSem);//mes: 5 dia: 4
		
	}
	public static void ejemplo3() {
//		Date miFecha = new Date();
//		DateFormat formato1=new SimpleDateFormat("dd/MM/yyyy");
//		String fecha2=formato1.format(miFecha);
//		System.out.println(fecha2);
	}
}
