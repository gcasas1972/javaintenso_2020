package clase5_generics;

import java.util.ArrayList;
import java.util.List;

public class TestGenerics {

	public static void main(String args[]) {
		//listaSimple();
		Integer[] valores= {5,10,15,20};
		Character[] valores2= {'A','B', 'C'};
		String[] valores3= {"Victor", "Cesar", "Sebastian"};
		//mostrar(valores);
		
		testGenericClass();
	}
	// EJEMPLO1
	public static void listaSimple() {
		// la lista esta TIPADA a STRING
		List<String> lista1=new ArrayList<String>();
		lista1.add("Tomas");		
		lista1.add("Victor");
		lista1.add("Luciano");
		// lista1.add(6); --> no me deja colocar otra cosa que no sea un string
		System.out.println(lista1); // dump , es un vuelco de la memoria de la lista
	}

	// EJEMPLO2
	// overload --TRADICIONAL
	public static void mostrar(Integer[] valores) {
		for (Integer elemento: valores) {
			System.out.println(elemento);
		}
	}
	
	public static void mostrar(Character[] valores) {
		for (Character elemento: valores) {
			System.out.println(elemento);
		}
	}
	
	// OVERLOAD, USO GENERICS	
	public static <T> void mostrar(T [] valores) {
		for (T elemento: valores) {
			System.out.println(elemento);
		}
	}

	// EJEMPLO3
	// test de Clases Generics
	public static void testGenericClass() {
		// sin clases generics
		CacheString miMensaje=new CacheString();
		CacheCamisa miCamisa= new CacheCamisa();
		
		miMensaje.agregar("empezo el invierno ");
		System.out.println(miMensaje.getMensaje());
		
		// y aplicando generics
		CacheAny<String> miMensaje1= new CacheAny<String>();
		CacheAny<Camisa> miCamisa1= new CacheAny<Camisa>();
		miMensaje1.agregar("hola que tal");
		System.out.println(miMensaje1.get());
		
		
	}
}
