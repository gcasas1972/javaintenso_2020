package clase5_object;

import java.util.Objects;

public class Empleado { // siempre cualquier clase es hija de OBJECT
// HEREDO ... los metodos de toString, equals, hashCode
	int id;
	String nombre;
	
	public Empleado(int id, String nombre) {
		this.id=id;
		this.nombre=nombre;
	}
	
	@Override
	public String toString() {
		return "Empleado id: " + this.id + ", " + " cuyo nombre es " + this.nombre;
	}
	
	@Override
	public boolean equals(Object o) {
		boolean resultado=false;
		if ((o != null) && (o instanceof Empleado)) {
			Empleado e = (Empleado) o;
			if ((e.id == this.id) && (e.nombre==this.nombre)) {
				resultado=true;
			}
		}
		return resultado;
	}
	
	@Override
	public int hashCode() {
		int hash=7;
		hash= 83 * hash + this.id;
		hash=83 * hash + Objects.hashCode(this.nombre);
		return hash; // 1331632335
	}
}
