package clase5_object;

public class TestObject {

	public static void main(String[] args) {
		//testToString();
		//testEquals();
		testHashCode();
	}
	
	public static void testToString() {
		Empleado e = new Empleado(101, "Bartolo");
		System.out.println(e.toString());		// clase5_object.Empleado@6acbcfc0 --> es lo que devuelve toString de Object
		Empleado e1 = new Empleado(102, "Mariano");
		System.out.println(e1);	// e1.toString(); // clase5_object.Empleado@5f184fc6
	}
	
	public static void testEquals() {
		Empleado e = new Empleado(101, "Bartolo");
		Empleado x= e;
		System.out.println(x.equals(e)); // da TRUE
		
		Empleado e1 = new Empleado(101, "Bartolo");
		System.out.println(e.equals(e1)); // da FALSE si uso el equals de OBJECT
	}

	public static void testHashCode() {
		Empleado e = new Empleado(101, "Bartolo");
	// hashCode() devuelve un nro entero
		System.out.println(e.hashCode()); // el de object sin override--> 1791741888
		Empleado e1 = new Empleado(104, "Bartolo");
		if (e.hashCode()== e1.hashCode()) {
			System.out.println("mismo hashcode");
		} else {
			System.out.println("distinto hashcode");
		}
	
	}
}
