package clase5_strings;

import java.util.StringTokenizer;

public class TestStrings {

	public static void main(String[] args) {
		ejemplo0();
		ejemplo1();
		ejemplo2();

	}
	public static void ejemplo0() {
		String frase="hola" + "chau";
		frase=frase+ "nos vemos";
		System.out.println(frase);
	}
	public static void ejemplo1() {
		StringBuilder str = new StringBuilder();
		str.append("hola");
		str.append("chau");
		
		String frase = str.toString();
		System.out.println(frase);
	}
	
	public static void ejemplo2() {
		StringTokenizer str= new StringTokenizer("mi nombre es Ale"," ");
		
		String datos="papas;batatas;zapallo;lechuga;mandarina;bananas";
		StringTokenizer str1= new StringTokenizer(datos, ";");
		while (str1.hasMoreElements()) {
			System.out.println(str1.nextToken());
		}
	}

}
