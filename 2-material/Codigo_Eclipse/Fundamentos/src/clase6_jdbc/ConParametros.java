package clase6_jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ConParametros {

	public static void main(String[] args) {
		String url="jdbc:mysql://localhost:3306/capgemini_arg";
		Connection con;
		PreparedStatement prestmt;
		String misql="Select * from coffees where price>?";
		
		try {
			con=DriverManager.getConnection(url, "root", "1234");
			prestmt=con.prepareStatement(misql);
			prestmt.setInt(1,8); // el parametro le cargo un valor externo
		    
			ResultSet rs=prestmt.executeQuery();
			
			System.out.println("Lista de Cafes con Precio>8");
			System.out.println("-----------------------------------");
			while(rs.next()) {
				System.out.println(rs.getString(1) + ", " + rs.getString(2)+  ", " + rs.getFloat(3));
			}
			
			prestmt.close();
			con.close();
			
		}catch(SQLException e1) {
			System.err.print("Query invalida");
		}
	}

}
