package clase6_jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class CrearCafe {

	public static void main(String[] args) {
		// 1er paso
		// chequear que el driver exista
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		}catch(java.lang.ClassNotFoundException e) {
			System.err.print("Driver no encontrado");
			System.err.print(e.getMessage());
		}
		
		// 2do paso
		// conectarse a la base de datos		
		String url="jdbc:mysql://localhost:3306/capgemini_arg";
		Connection con;
		
		// 3er paso
		// dialogar con las tablas	
		Statement stmt;
		String misql="create table coffees (cof_name varchar(32), " +
													"sup_id int, " +
													"price float, " +
													"sales int, " +
													"total int)";
		
		try {
			con=DriverManager.getConnection(url, "root", "1234");
			stmt=con.createStatement();
			stmt.executeUpdate(misql);
			stmt.close();
			con.close();
			
		}catch(SQLException e1) {
			System.err.print("Query invalida");
		}catch(Exception e) {
			System.err.print("BD/usu/pass NO encontrado");
			System.err.print(e.getMessage());
		}
	
	}

}
