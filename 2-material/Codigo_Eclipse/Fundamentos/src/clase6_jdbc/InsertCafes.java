package clase6_jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class InsertCafes {

	public static void main(String[] args) {
		String url="jdbc:mysql://localhost:3306/capgemini_arg";
		Connection con;
		Statement stmt;
		
		try {
			con=DriverManager.getConnection(url, "root", "1234");
			stmt=con.createStatement();
			stmt.executeUpdate("insert into coffees values ('Colombiano',101,7.99,0,0)");
			stmt.executeUpdate("insert into coffees values ('Frances Tostado',150,9.99,0,0)");
			stmt.executeUpdate("insert into coffees values ('Espresso',101,5.99,0,0)");
			stmt.executeUpdate("insert into coffees values ('Colombiano Decaf',150,8.99,0,0)");
			stmt.executeUpdate("insert into coffees values ('CafeLatte',101,7.99,0,0)");
			
			stmt.close();
			con.close();
			
		}catch(SQLException e1) {
			System.err.print("Query invalida");
		}
	}

}
