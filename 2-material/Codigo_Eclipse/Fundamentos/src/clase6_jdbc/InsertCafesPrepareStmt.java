package clase6_jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public class InsertCafesPrepareStmt {

	public static void main(String[] args) {
		String url="jdbc:mysql://localhost:3306/capgemini_arg";
		Connection con;
		PreparedStatement stmt;
		String misql="insert into coffees (cof_name, sup_id, price) values (?,?,?)";
	 
		try {
			con=DriverManager.getConnection(url, "root", "1234");
			stmt=con.prepareStatement(misql);
			stmt.setString(1, "Caramel Macchiato");
			stmt.setInt(2, 101);
			stmt.setFloat(3, 11.99f);
			int i= stmt.executeUpdate();
			if(i>0) {
				System.out.println("alta exitosa");
			} else {
				System.out.println("no se pudo realizar el alta");
			}
			stmt.close();
			con.close();
			
		}catch(SQLException e1) {
			System.err.print("Query invalida");
		}
	}

}
