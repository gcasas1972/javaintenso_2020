package clase6_jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ListaCafes {

	public static void main(String[] args) {
		String url="jdbc:mysql://localhost:3306/capgemini_arg";
		Connection con;
		Statement stmt;
		String misql="Select * from coffees";
		
		try {
			con=DriverManager.getConnection(url, "root", "1234");
		    stmt=con.createStatement();
			ResultSet rs=stmt.executeQuery(misql);
			
			System.out.println("Lista de Cafes");
			System.out.println("-----------------------------------");
			while(rs.next()) {
				System.out.println(rs.getString("cof_name") + ", " + rs.getFloat(3));
			}
			
			stmt.close();
			con.close();
			
		}catch(SQLException e1) {
			System.err.print("Query invalida");
		}
	}

}
