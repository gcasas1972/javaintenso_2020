package clase6_jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

public class PrintColumns {

	public static void main(String[] args) {
		String url="jdbc:mysql://localhost:3306/capgemini_arg";
		Connection con;
		Statement stmt;
		String misql="Select * from coffees";
		try {
			con=DriverManager.getConnection(url, "root", "1234");
		    stmt=con.createStatement();
			ResultSet rs=stmt.executeQuery(misql);
			ResultSetMetaData rsmd=rs.getMetaData();
			PrintColumnTypes.printColTypes(rsmd);			
			System.out.println("**************************");
			int nroCols= rsmd.getColumnCount();
			
			System.out.println("Lista de Cafes");
			System.out.println("-----------------------------------");
			// imprime el nombre de los campos
			for(int i=1;i<=nroCols;i++) {
				if(i>1) System.out.print(", ");
				String columnName=rsmd.getColumnName(i);
				System.out.print(columnName);
			}
			System.out.println("-----------------------------------");
			System.out.println("");		
			while(rs.next()) {
				for(int i=1; i<=nroCols; i++) {
					if(i>1) System.out.print(", ");
					String columnName=rs.getString(i);
					System.out.print(columnName);
			    }
				System.out.println("");
			}
			
			stmt.close();
			con.close();
			
		}catch(SQLException e1) {
			System.err.print("Query invalida");
		}
	}

}
