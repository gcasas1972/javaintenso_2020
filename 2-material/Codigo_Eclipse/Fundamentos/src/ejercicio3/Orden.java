package ejercicio3;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Orden {
String status="PROCESADA";
LocalDate fecha;

// OVERLOAD --> sobrecarga en metodos constructores
//metodo constructor predeterminado
public Orden() {
	this.fecha=LocalDate.now();
} 

public Orden(String status) {
	this.status=status;
	this.fecha=LocalDate.now();
}

// OVERLOAD
public void verDiaActual() {
		System.out.println(this.fecha.now());
}
public void verDiaActual(String saludo) {
	System.out.println(this.fecha.now());
}
public void verDiaActual(String saludo, String nombre) {
	System.out.println(this.fecha.now());
}
public void verDiaActual(String saludo, int hora) {
	System.out.println(this.fecha.now());
}
}
