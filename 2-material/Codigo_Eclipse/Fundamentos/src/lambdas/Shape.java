package lambdas;

import java.util.*;

public class Shape {
private String name;
private double diameter;

public Shape(String name, double diameter) {
	this.setName(name);
	this.setDiameter(diameter);
}
public String toString() {
	return String.format("%s [%.2f]", getName(),getDiameter());
}

public double getDiameter() {
	return diameter;
}
public void setDiameter(double diameter) {
	this.diameter = diameter;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}

public static void main(String args[]) {
	List<Shape> shapes=new ArrayList<>();
	shapes.add(new Shape("S1",286));
	shapes.add(new Shape("S2",512));
	shapes.add(new Shape("S3",268));
	shapes.add(new Shape("S4",258));
	shapes.add(new Shape("S5",431));
	shapes.add(new Shape("S6",289));
	
	Shape s=shapes.stream().reduce(shapes.get(0),(a,b)->
	a.getDiameter()>b.getDiameter()?a:b);
	System.out.println(s);
}
}
