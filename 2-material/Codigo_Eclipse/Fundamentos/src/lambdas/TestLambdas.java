package lambdas;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.Comparator;

public class TestLambdas {
public static void main(String args[]) {
	//ejemplo1();
	//ejemplo2();
	//ejemplo3();
	ejemplo4();
}

public static void imprimir(String str, StringFunction formato) {
	// llamamos al metodo de la interfaz, y ejecutamos la expresion lambda
	String rpta=formato.run(str);
	System.out.println(rpta);
}

public static void ejemplo4() {
	// usamos una expresion lambda en un metodo, el metodo debe tener un 
	// parametro con una interfaz de metodo UNICO como tipo
	StringFunction exclamacion=(s)-> s +"!";
	StringFunction pregunta=(s)-> s +"?";
	imprimir("holaaaa ", exclamacion);
	imprimir("chauuuu ", pregunta);
}

public static void ordenar(List<Pais> list) {
	Collections.sort(list, new Comparator<Pais>(){
		@Override
		public int compare(Pais p1, Pais p2) {
			return p1.nombre.compareTo(p2.nombre);
		}
	});
}

public static void ordernarConLambdas(List<Pais> list) {
	Collections.sort(list, (p1,p2)->{ return p1.nombre.compareTo(p2.nombre);});
}

public static void ejemplo3() {
	// ordenar listas con expresiones Lambdas
	// comparar el nombre de 2 paises
	Pais pais1=new Pais("Italia");
	Pais pais2=new Pais("Francia");
	Pais pais3=new Pais("Espa�a");
	Pais pais4=new Pais("Argentina");
	Pais pais5=new Pais("China");
	
	List<Pais> paises=new ArrayList<Pais>();
	paises.add(pais1);
	paises.add(pais2);
	paises.add(pais3);
	paises.add(pais4);
	paises.add(pais5);
	
	paises.forEach((p)->{System.out.print(p.nombre +", ");});
	System.out.println();
	System.out.println("paises ordenados de forma tradicional");
	ordenar(paises);
	paises.forEach((p)->{System.out.print(p.nombre +", ");});
	System.out.println();
	
	System.out.println("paises ordenados con Lambdas");
	Pais pais6=new Pais("Dinamarca");
	paises.add(pais6);
	ordernarConLambdas(paises);
	paises.forEach((p)->{System.out.print(p.nombre +", ");});
	System.out.println();
	
}

public static void ejemplo2() {
	// el uso de la interfaz Consumer, 
	//para almacenar una expresion Lambda en una variable
	ArrayList<Integer> numeros=new ArrayList<Integer>();
	numeros.add(5);
	numeros.add(9);
	numeros.add(1);
	numeros.add(7);
	Consumer<Integer> metodo=(n)->{System.out.println(n);};
	numeros.forEach(metodo);
	
}

public static void ejemplo1() {
	// antes
	for (Integer numero: Arrays.asList(1,2,3,4,5,6,7,8,9,10)) {
		System.out.println(numero + " ");
	}
	System.out.println("con Lambdas");
	// despues
	Arrays.asList(1,2,3,4,5,6,7,8,9,10).forEach( 
			n -> System.out.println(n + " "));
	System.out.println("otra forma de Lambdas");
	// otra forma
	ArrayList<Integer> numeros=new ArrayList<Integer>();
	numeros.add(5);
	numeros.add(9);
	numeros.add(1);
	numeros.add(7);
	numeros.forEach((n)->System.out.println(n + " "));
	System.out.println("otra forma de Lambdas");
	numeros.forEach(System.out::println); // otra forma de expresion Lambda
}

}
