package com.elca.cursos.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.elca.cursos.model.Curso;

public interface CursoRepositorio extends JpaRepository<Curso, Long>{
// dialoga hacia el backend, hacia la BD
}
