package com.elca.cursos.service;

import java.util.List;
import org.springframework.data.domain.Page;
import com.elca.cursos.model.Curso;

public interface CursoService {
// esta interfaz define el dialogo hacia el frontEnd
	List<Curso> getAllCursos();
	void saveCurso(Curso curso);
	Curso getCursoById(long id);
	void deleteCursoById(long id);
	
	Page<Curso> findPaginated(int pageNum, int pageSize, String sortField, String sortDirection);
}
