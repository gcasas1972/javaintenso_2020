package com.elca.cursos.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.elca.cursos.model.Curso;
import com.elca.cursos.repository.CursoRepositorio;

@Service
public class CursoServiceImp implements CursoService{
	@Autowired
	private CursoRepositorio cursoRepositorio;
	
	@Override
	public List<Curso> getAllCursos() {
		return cursoRepositorio.findAll();
	}

	@Override
	public void saveCurso(Curso curso) {
		cursoRepositorio.save(curso);	
	}

	@Override
	public Curso getCursoById(long id) {
		Optional<Curso> optionalCurso=cursoRepositorio.findById(id);
		Curso curso=null;
		if(optionalCurso.isPresent()) {
			curso=optionalCurso.get();
		}else {
			throw new RuntimeException("el curso no se encuentra nro: " + id);
		}
		return curso;
	}

	@Override
	public void deleteCursoById(long id) {
		this.cursoRepositorio.deleteById(id);
		
	}

	@Override
	public Page<Curso> findPaginated(int pageNum, int pageSize, String sortField, String sortDirection) {
		Sort sort= sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name()) ? 
				Sort.by(sortField).ascending() :
				Sort.by(sortField).descending();
		Pageable pageable=PageRequest.of(pageNum -1, pageSize, sort);
		return this.cursoRepositorio.findAll(pageable);
	}

}
