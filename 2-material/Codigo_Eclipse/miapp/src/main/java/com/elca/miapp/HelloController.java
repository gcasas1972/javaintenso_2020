package com.elca.miapp;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

	@GetMapping("/")
	public String index() {
		return "Saludos desde Spring Boot!!!";
	}
}

/*
@RestController:
significa que esta listo para gestionar peticiones web para un app MVC de Spring
es una combinacion de @Controller (es un Bean) y @ResponseBody


*/