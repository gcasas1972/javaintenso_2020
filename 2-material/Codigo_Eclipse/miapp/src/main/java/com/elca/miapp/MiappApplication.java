package com.elca.miapp;

import java.util.Arrays;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class MiappApplication {

	public static void main(String[] args) {
		SpringApplication.run(MiappApplication.class, args);
	}
	
	@Bean
	public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
		return args -> {
			System.out.println("Vemos la lista de beans que tiene Spring Boot");
			String[] beanNames=ctx.getBeanDefinitionNames();
			Arrays.sort(beanNames);
			for (String beanName : beanNames) {
				System.out.println(beanName);
			}
		};
	}
}
/*
@SpringBootApplication:
agrega las siguientes annotations del viejo Spring Framework ->

@Configuration:
una clase fuente de definicion de los beans para el contexto de la aplicacion
@EnableAutoConfiguration:
le dice a Spring Boot que arranque las beans en el seteo del classpath
Por ejemplo, antes en un spring-mvc, codificabamos un DispatcherServlet ... que era el que inicializaba la app
@ComponentScan:
barrer algun package, en busqueda de Beans, para levantarlas en memoria
Beans.... @Bean, @Controller, @Service, @Component, @Entity


*/