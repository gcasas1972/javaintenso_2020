use capgemini_arg;
create table  wf_currencies 
   (currency_code varchar(7) primary key, 
	currency_name varchar(40) not null, 
	comments varchar(150)
   );

create table  wf_languages
   (language_id smallint primary key, 
	language_name varchar(50)  not null	
   );

create table  wf_world_regions
   (region_id smallint primary key, 
	region_name varchar(35) not null
   );

create table  wf_countries
   (country_id smallint primary key, 
	region_id smallint not null, 
	country_name varchar(70) not null, 
	country_translated_name varchar(40), 
	location varchar(90), 
	capitol varchar(50), 
	area bigint, 
	coastline int, 
	lowest_elevation int, 
	lowest_elev_name varchar(70), 
	highest_elevation int, 
	highest_elev_name varchar(50), 
	date_of_independence varchar(30), 
	national_holiday_name varchar(200), 
	national_holiday_date varchar(30), 
	population bigint, 
	population_growth_rate varchar(10), 
	life_expect_at_birth decimal(6,2), 
	median_age decimal(6,2), 
	airports int, 
	climate varchar(1000), 
	fips_id char(2), 
	internet_extension varchar(3), 
	flag bit, 
	currency_code varchar(7) not null
   );

CREATE TABLE  wf_spoken_languages
   (country_id int, 
	language_id int, 
	official VARCHAR(2), 
	comments VARCHAR(512), 
	CONSTRAINT wf_spoken_languages_pk PRIMARY KEY (COUNTRY_ID, LANGUAGE_ID)
   );

