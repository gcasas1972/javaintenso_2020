use capgemini_arg;
CREATE TABLE  F_CUSTOMERS
   (ID int,
    FIRST_NAME  VARCHAR(25) NOT NULL ,
    LAST_NAME VARCHAR(35) NOT NULL ,
    ADDRESS VARCHAR(50) NOT NULL ,
    CITY VARCHAR(30) NOT NULL ,
    STATE VARCHAR(20)  NOT NULL ,
    ZIP int NOT NULL ,
    PHONE_INT VARCHAR(10) NOT NULL ,
     CONSTRAINT F_CSR_ID_PK PRIMARY KEY (ID)
   );

CREATE TABLE  F_PROMOTIONAL_MENUS
   (CODE VARCHAR(3),
    NAME VARCHAR(30)  NOT NULL ,
    START_DATE DATE  NOT NULL ,
    END_DATE DATE,
    GIVE_AWAY VARCHAR(80),
     CONSTRAINT F_PMU_CODE_PK PRIMARY KEY (CODE)
   );

CREATE TABLE  F_REGULAR_MENUS
   (CODE VARCHAR(3),
    TYPE VARCHAR(30)  NOT NULL ,
    HOURS_SERVED VARCHAR(30) NOT NULL ,
     CONSTRAINT F_RMU_CODE_PK PRIMARY KEY (CODE)
   );

CREATE TABLE  F_FOOD_ITEMS
   (FOOD_ITEM_INT int,
    DESCRIPTION VARCHAR(100) NOT NULL ,
    PRICE int NOT NULL ,
    REGULAR_CODE VARCHAR(3),
    PROMO_CODE VARCHAR(3),
     CONSTRAINT F_FIM_FOOD_ITEM_INT_PK PRIMARY KEY (FOOD_ITEM_INT)
   );

CREATE TABLE  F_STAFFS
   (ID int,
    FIRST_NAME VARCHAR(25)  NOT NULL ,
    LAST_NAME VARCHAR(35)  NOT NULL ,
    BIRTHDATE DATE NOT NULL ,
    SALARY int NOT NULL ,
    OVERTIME_RATE int,
    TRAINING VARCHAR(50),
    STAFF_TYPE VARCHAR(20)  NOT NULL ,
    MANAGER_ID int,
    MANAGER_BUDGET int,
    MANAGER_TARGET int,
     CONSTRAINT F_STF_ID_PK PRIMARY KEY (ID)
   );

CREATE TABLE  F_ORDERS
   (ORDER_INT int,
    ORDER_DATE DATE NOT NULL ,
    ORDER_TOTAL int,
    CUST_ID int  NOT NULL ,
    STAFF_ID int  NOT NULL ,
     CONSTRAINT F_ODR_ORDER_INT_PK PRIMARY KEY (ORDER_INT)
   );

CREATE TABLE  F_ORDER_LINES
   (ORDER_INT int,
    FOOD_ITEM_INT int,
    QUANTITY int NOT NULL ,
     CONSTRAINT F_OLE_PK PRIMARY KEY (ORDER_INT, FOOD_ITEM_INT)
   );

CREATE TABLE  F_SHIFTS
   (CODE int,
    DESCRIPTION VARCHAR(100) NOT NULL ,
     CONSTRAINT F_SFT_CODE_PK PRIMARY KEY (CODE)
   );

CREATE TABLE  F_SHIFT_ASSIGNMENTS
   (CODE int,
    ID int,
    SHIFT_ASSIGN_DATE DATE NOT NULL ,
     CONSTRAINT F_SAT_PK PRIMARY KEY (CODE, ID)
   );


# Populate f_customer table
INSERT INTO f_customers(id,first_name,last_name,address,city,state,zip,phone_INT)
VALUES(123,'Cole','Bee','123 Main Street','Orlando','FL',32838,'4075558234');
INSERT INTO f_customers(id,first_name,last_name,address,city,state,zip,phone_INT)
VALUES(456,'Zoe','Twee','1009 Oliver Avenue','Boston','MA',12889,'7098675309');
# Populate f_promotional_menus table
INSERT INTO f_promotional_menus(code,name,start_date,end_date,give_away)
VALUES('100','Back to School','2004-09-01','2004-09-30','ballpen and highlighter');
INSERT INTO f_promotional_menus(code,name,start_date,end_date,give_away)
VALUES('110','Valentines Special','2004-02-10','2004-02-15','small box of chocolates');
# Populate f_regular_menus table
INSERT INTO f_regular_menus(code,type,hours_served)
VALUES('10','Breakfast','6-11am');
INSERT INTO f_regular_menus(code,type,hours_served)
VALUES('20','Lunch/Dinner','11-9pm');
# Populate f_food_items table
INSERT INTO f_food_items(food_item_INT,description,price,regular_code,promo_code)
VALUES(90,'Fries',1.09,'20',NULL);
INSERT INTO f_food_items(food_item_INT,description,price,regular_code,promo_code)
VALUES(93,'Strawberry Shake',3.59,NULL,'110');
# Populate f_staffs table
INSERT INTO f_staffs(id,first_name,last_name,birthdate,salary,overtime_rate,training,staff_type,manager_id,manager_budget,manager_target)
VALUES(12,'Sue','Doe','1980-07-01',6.75,10.25,NULL,'Order Taker',19,NULL,NULL);
INSERT INTO f_staffs(id,first_name,last_name,birthdate,salary,overtime_rate,training,staff_type,manager_id,manager_budget,manager_target)
VALUES(9,'Bob','Miller','1979-03-19',10,NULL,'Grill','Cook',19,NULL,NULL);
INSERT INTO f_staffs(id,first_name,last_name,birthdate,salary,overtime_rate,training,staff_type,manager_id,manager_budget,manager_target)
VALUES(19,'Monique','Tuttle','1969-03-30',60,NULL,NULL,'Manager',NULL,50000,70000);
# Populate f_orders table
INSERT INTO f_orders(order_INT,order_date,order_total,cust_id,staff_id)
VALUES(5678,'2002-12-10',103.02,123,12);
# Populate f_order_lines table
INSERT INTO f_order_lines(order_INT,food_item_INT,quantity)
VALUES(5678,90,2);
# Populate f_shifts table
INSERT INTO f_shifts(code,description)
VALUES(1,'8am to 12pm');
INSERT INTO f_shifts(code,description)
VALUES(2,'6pm to 10pm');
# Populate f_shift_assignments table
INSERT INTO f_shift_assignments(code,id,shift_assign_date)
VALUES(1,12,'2004-05-06');

CREATE TABLE D_CDS
   (CD_INT int,
    TITLE VARCHAR(50)  NOT NULL ,
    PRODUCER VARCHAR(50)  NOT NULL ,
    YEAR VARCHAR(4)  NOT NULL ,
     CONSTRAINT D_CDS_CD_INT_PK PRIMARY KEY (CD_INT)
   );

CREATE TABLE  D_CLIENTS
   (CLIENT_INT int,
    FIRST_NAME VARCHAR(25) NOT NULL ,
    LAST_NAME VARCHAR(30)  NOT NULL ,
    PHONE int NOT NULL ,
    EMAIL VARCHAR(50),
     CONSTRAINT D_CLT_CLIENT_INT_PK PRIMARY KEY (CLIENT_INT)
   );

CREATE TABLE  D_PACKAGES
   (CODE int,
    LOW_RANGE int NOT NULL ,
    HIGH_RANGE int NOT NULL ,
     CONSTRAINT D_PKE_CODE_PK PRIMARY KEY (CODE)
   );

CREATE TABLE  D_THEMES
   (CODE int,
    DESCRIPTION VARCHAR(100) NOT NULL ,
     CONSTRAINT D_TME_CODE_PK PRIMARY KEY (CODE)
   );

CREATE TABLE  D_VENUES
   (ID int,
    LOC_TYPE  VARCHAR(30)  NOT NULL ,
    ADDRESS VARCHAR(100) NOT NULL ,
    RENTAL_FEE VARCHAR(50) NOT NULL ,
    COMMENTS VARCHAR(100),
     CONSTRAINT D_VNE_ID_PK PRIMARY KEY (ID)
   );

CREATE TABLE  D_EVENTS
   (ID int,
    NAME VARCHAR(50)  NOT NULL ,
    EVENT_DATE DATE NOT NULL ,
    DESCRIPTION VARCHAR(50),
    COST int NOT NULL ,
    VENUE_ID int NOT NULL ,
    PACKAGE_CODE int NOT NULL ,
    THEME_CODE int NOT NULL ,
    CLIENT_INT int NOT NULL ,
     CONSTRAINT D_EVE_ID_PK PRIMARY KEY (ID)
   );

CREATE TABLE  D_PARTNERS
   (ID int,
    FIRST_NAME VARCHAR(25)  NOT NULL ,
    LAST_NAME VARCHAR(30) NOT NULL ,
    EXPERTISE VARCHAR(25),
    SPECIALTY VARCHAR(25),
    AUTH_EXPENSE_AMT int,
    MANAGER_ID int,
    PARTNER_TYPE VARCHAR(25) NOT NULL ,
     CONSTRAINT D_PTR_ID_PK PRIMARY KEY (ID)
   );

CREATE TABLE  D_JOB_ASSIGNMENTS
   (PARTNER_ID int,
    EVENT_ID int,
    JOB_DATE DATE NOT NULL ,
    STATUS VARCHAR(50),
     CONSTRAINT D_JAT_PK PRIMARY KEY (PARTNER_ID, EVENT_ID)
   );

CREATE TABLE  D_TYPES
   (CODE int,
    DESCRIPTION VARCHAR(50) NOT NULL ,
     CONSTRAINT D_TPE_CODE_PK PRIMARY KEY (CODE)
   );

CREATE TABLE  D_SONGS
   (ID int,
    TITLE VARCHAR(50) NOT NULL ,
    DURATION VARCHAR(20),
    ARTIST VARCHAR(20),
    TYPE_CODE int NOT NULL ,
     CONSTRAINT D_SNG_ID_PK PRIMARY KEY (ID)
   );

CREATE TABLE  D_PLAY_LIST_ITEMS
   (EVENT_ID int,
    SONG_ID int,
    COMMENTS VARCHAR(80),
     CONSTRAINT D_PLM_PK PRIMARY KEY (EVENT_ID, SONG_ID)
   );

CREATE TABLE  D_TRACK_LISTINGS
   (SONG_ID int,
    CD_INT int,
    TRACK int NOT NULL ,
     CONSTRAINT D_TLG_PK PRIMARY KEY (SONG_ID, CD_INT)
   );

-- populate tabes
INSERT INTO d_cds(cd_INT,title,producer,year)
VALUES(90,'The Celebrants Live in Concert','Old Town Records','1997');
INSERT INTO d_cds(cd_INT,title,producer,year)
VALUES(91,'Party Music for All Occasions','The Music Man','2000');
INSERT INTO d_cds(cd_INT,title,producer,year)
VALUES(92,'Back to the Shire','Middle Earth Records','2002');
INSERT INTO d_cds(cd_INT,title,producer,year)
VALUES(93,'Songs from My Childhood','Old Town Records','1999');
INSERT INTO d_cds(cd_INT,title,producer,year)
VALUES(94,'Carpe Diem','R & B Inc.','2000');
INSERT INTO d_cds(cd_INT,title,producer,year)
VALUES(95,'Here Comes the Bride','The Music Man','2001');
INSERT INTO d_cds(cd_INT,title,producer,year)
VALUES(96,'Graduation Songbook','Tunes Are Us','1998');
INSERT INTO d_cds(cd_INT,title,producer,year)
VALUES(98,'Whirled Peas','Old Town Records','2004');

INSERT INTO d_clients(client_INT,first_name,last_name,phone,email)
VALUES(5922,'Hiram','Peters',3715832,'hpeters@yahoo.com');
INSERT INTO d_clients(client_INT,first_name,last_name,phone,email)
VALUES(5857,'Serena','Jones',7035335,'serena.jones@jones.com');
INSERT INTO d_clients(client_INT,first_name,last_name,phone,email)
VALUES(6133,'Lauren','Vigil',4072220,'lbv@lbv.net');

INSERT INTO d_packages(code,low_range,high_range)
VALUES(79,500,2500);
INSERT INTO d_packages(code,low_range,high_range)
VALUES(87,2501,5000);
INSERT INTO d_packages(code,low_range,high_range)
VALUES(112,5001,10000);
INSERT INTO d_packages(code,low_range,high_range)
VALUES(200,10001,15000);

INSERT INTO d_themes(code,description)
VALUES(200,'Tropical');
INSERT INTO d_themes(code,description)
VALUES(220,'Carnival');
INSERT INTO d_themes(code,description)
VALUES(240,'Sixties');
INSERT INTO d_themes(code,description)
VALUES(110,'Classic');
INSERT INTO d_themes(code,description)
VALUES(198,'Mardi Gras');
INSERT INTO d_themes(code,description)
VALUES(454,'Eighties');
INSERT INTO d_themes(code,description)
VALUES(340,'Football');
INSERT INTO d_themes(code,description)
VALUES(502,'Fairy Tale');

INSERT INTO d_venues(id,loc_type,address,rental_fee,comments)
VALUES(100,'Private Home','52 West End Drive, Los Angeles, CA 90210','0','Large kitchen, spacious lawn');
INSERT INTO d_venues(id,loc_type,address,rental_fee,comments)
VALUES(105,'Private Home','123 Magnolia Road, Hudson, N.Y. 11220','0','3 level townhouse, speakers on all floors');
INSERT INTO d_venues(id,loc_type,address,rental_fee,comments)
VALUES(101,'Private Home','4 Primrose Lane, Chevy Chase, MD 22127','0','Gazebo, multi-level deck');
INSERT INTO d_venues(id,loc_type,address,rental_fee,comments)
VALUES(95,'School Hall','4 Mahogany Drive, Boston, MA 10010','75/hour','School closes at 10pm');
INSERT INTO d_venues(id,loc_type,address,rental_fee,comments)
VALUES(99,'National Park','87 Park Avenue, San Diego, CA 28978','400/flat fee','Bring generators');
INSERT INTO d_venues(id,loc_type,address,rental_fee,comments)
VALUES(220,'Hotel','200 Pennsylvania Ave, Washington D.C. 09002','300/per person','Classy affair, tight security, private entrance for vendors');


INSERT INTO d_events(client_INT,id,name,event_date,description,cost,venue_id,package_code,theme_code)
VALUES(5922,100,'Peters Graduation','2004-05-14','Party for 200, red, white, blue motif',8000,100,112,200);
INSERT INTO d_events(client_INT,id,name,event_date,description,cost,venue_id,package_code,theme_code)
VALUES(6133,105,'Vigil wedding','2004-04-28','Black tie at Four Season hotel',10000,220,200,200);

INSERT INTO d_partners(id,first_name,last_name,expertise,specialty,auth_expense_amt,manager_id,partner_type)
VALUES(11,'Jennifer','cho','Weddings','All Types',NULL,33,'Wedding Coordinator');
INSERT INTO d_partners(id,first_name,last_name,expertise,specialty,auth_expense_amt,manager_id,partner_type)
VALUES(22,'Jason','Tsang',NULL,'Hip Hop',NULL,33,'Disk Jockey');
INSERT INTO d_partners(id,first_name,last_name,expertise,specialty,auth_expense_amt,manager_id,partner_type)
VALUES(33,'Allison','Plumb','Event Planning',NULL,300000,33,'Manager');

INSERT INTO d_job_assignments(partner_id,event_id,job_date,status)
VALUES(11,105,'2004-02-02','Visited');

INSERT INTO d_types(code,description)
VALUES(1,'Jazz');
INSERT INTO d_types(code,description)
VALUES(12,'Pop');
INSERT INTO d_types(code,description)
VALUES(40,'Reggae');
INSERT INTO d_types(code,description)
VALUES(88,'Country');
INSERT INTO d_types(code,description)
VALUES(77,'New Age');

INSERT INTO d_songs(id,title,duration,artist,type_code)
VALUES(45,'Its Finally Over','5 min','The Hobbits',12);
INSERT INTO d_songs(id,title,duration,artist,type_code)
VALUES(46,'Im Going to Miss My Teacher','2 min','Jane Pop',12);
INSERT INTO d_songs(id,title,duration,artist,type_code)
VALUES(47,'Hurrah for Today','3 min','The Jubilant Trio',77);
INSERT INTO d_songs(id,title,duration,artist,type_code)
VALUES(48,'Meet Me At the Altar','6 min','Bobby West',1);
INSERT INTO d_songs(id,title,duration,artist,type_code)
VALUES(49,'Lets Celebrate','8 min','The Celebrants',77);
INSERT INTO d_songs(id,title,duration,artist,type_code)
VALUES(50,'All These Years','10 min','Diana Crooner',88);

INSERT INTO d_play_list_items(event_id,song_id,comments)
VALUES(100,45,'Play late');
INSERT INTO d_play_list_items(event_id,song_id,comments)
VALUES(100,46,NULL);
INSERT INTO d_play_list_items(event_id,song_id,comments)
VALUES(100,47,'Play early');
INSERT INTO d_play_list_items(event_id,song_id,comments)
VALUES(105,48,'Play after cake cutting');
INSERT INTO d_play_list_items(event_id,song_id,comments)
VALUES(105,49,'Play first');
INSERT INTO d_play_list_items(event_id,song_id,comments)
VALUES(105,47,'Play for the father');

INSERT INTO d_track_listings(song_id,cd_INT,track)
VALUES(45,92,1);
INSERT INTO d_track_listings(song_id,cd_INT,track)
VALUES(46,93,1);
INSERT INTO d_track_listings(song_id,cd_INT,track)
VALUES(47,91,2);
INSERT INTO d_track_listings(song_id,cd_INT,track)
VALUES(48,95,5);
INSERT INTO d_track_listings(song_id,cd_INT,track)
VALUES(49,91,3);

CREATE TABLE   REGIONS 
   ( REGION_ID  int NOT NULL ,
     REGION_NAME  VARCHAR(25),
    CONSTRAINT  REG_ID_PK  PRIMARY KEY ( REGION_ID )
   );
   
CREATE TABLE   COUNTRIES 
   ( COUNTRY_ID  CHAR(2) NOT NULL ,
     COUNTRY_NAME  VARCHAR(40),
     REGION_ID  int,
     CONSTRAINT  COUNTRY_C_ID_PK  PRIMARY KEY ( COUNTRY_ID ) 
   );

   
CREATE TABLE   LOCATIONS 
   ( LOCATION_ID  INT,
     STREET_ADDRESS  VARCHAR(40),
     POSTAL_CODE  VARCHAR(12),
     CITY  VARCHAR(30)  NOT NULL  ,
     STATE_PROVINCE  VARCHAR(25),
     COUNTRY_ID  CHAR(2),
    CONSTRAINT  LOC_ID_PK  PRIMARY KEY ( LOCATION_ID )
   );
   

CREATE TABLE   DEPARTMENTS 
   ( DEPARTMENT_ID  INT,
     DEPARTMENT_NAME  VARCHAR(30) NOT NULL  ,
     MANAGER_ID  INT,
     LOCATION_ID  INT,
     CONSTRAINT  DEPT_ID_PK  PRIMARY KEY ( DEPARTMENT_ID )
     
   );
 
	
CREATE TABLE   JOBS 
   ( JOB_ID  VARCHAR(10),
     JOB_TITLE  VARCHAR(35) NOT NULL  ,
     MIN_SALARY  INT,
     MAX_SALARY  INT,
     CONSTRAINT  JOB_ID_PK  PRIMARY KEY ( JOB_ID )
     
   );
	
CREATE TABLE   EMPLOYEES 
   ( EMPLOYEE_ID  INT,
     FIRST_NAME  VARCHAR(20),
     LAST_NAME  VARCHAR(25)   NOT NULL  ,
     EMAIL  VARCHAR(25)   NOT NULL  ,
     PHONE_INT  VARCHAR(20),
     HIRE_DATE  DATE  NOT NULL  ,
     JOB_ID  VARCHAR(10)  NOT NULL  ,
     SALARY  INT,
     COMMISSION_PCT  INT,
     MANAGER_ID  INT,
     DEPARTMENT_ID  INT,
	 BONUS  VARCHAR(5),
     CONSTRAINT  EMP_SALARY_MIN  CHECK (salary > 0)  ,
     CONSTRAINT  EMP_ID_PK  PRIMARY KEY ( EMPLOYEE_ID )
     ,
     CONSTRAINT  EMP_EMAIL_UK  UNIQUE ( EMAIL )
     
  );
  

CREATE TABLE   JOB_GRADES 
   ( GRADE_LEVEL  VARCHAR(3),
     LOWEST_SAL  INT,
     HIGHEST_SAL  INT
   );

CREATE TABLE   JOB_HISTORY 
   ( EMPLOYEE_ID  INT  NOT NULL  ,
     START_DATE  DATE  NOT NULL  ,
     END_DATE  DATE  NOT NULL  ,
     JOB_ID  VARCHAR(10)   NOT NULL  ,
     DEPARTMENT_ID  INT,
     CONSTRAINT  JHIST_DATE_INTERVAL  CHECK (end_date > start_date)  ,
     CONSTRAINT  JHIST_EMP_ID_ST_DATE_PK  PRIMARY KEY ( EMPLOYEE_ID ,  START_DATE )
     
   );


# populate regions table
INSERT INTO regions (region_id, region_name)
Values(1,'Europe');
INSERT INTO regions (region_id, region_name)
Values(2,'Americas');
INSERT INTO regions (region_id, region_name)
Values(3,'Asia');
INSERT INTO regions (region_id, region_name)
Values(4,'Middle East and Africa');

# populate countries table
INSERT INTO countries (country_id, country_name, region_id)
Values('CA','Canada',2);
INSERT INTO countries (country_id, country_name, region_id)
Values('DE','Germany',1);
INSERT INTO countries (country_id, country_name, region_id)
Values('UK','United Kingdom',1);
INSERT INTO countries (country_id, country_name, region_id)
Values('US','United States of America',2);

# populate locations table
INSERT INTO locations (location_id, street_address, postal_code, city, state_province, country_id)
Values(1800,'460 Bloor St. W.','ON M5S 1X8','Toronto','Ontario','CA');
INSERT INTO locations (location_id, street_address, postal_code, city, state_province, country_id)
Values(2500,'Magdalen Centre, The Oxford Science Park','OX9 9ZB','Oxford','Oxford','UK');
INSERT INTO locations (location_id, street_address, postal_code, city, state_province, country_id)
Values(1400,'2014 Jabberwocky Rd','26192','Southlake','Texas','US');
INSERT INTO locations (location_id, street_address, postal_code, city, state_province, country_id)
Values(1500,'2011 Interiors Blvd','99236','South San Francisco','California','US');
INSERT INTO locations (location_id, street_address, postal_code, city, state_province, country_id)
Values(1700,'2004 Charade Rd','98199','Seattle','Washington','US');

# populate departments table
INSERT INTO departments (department_id, department_name, manager_id, location_id)
Values(10,'Administration',200,1700);
INSERT INTO departments (department_id, department_name, manager_id, location_id)
Values(20,'Marketing',201,1800);
INSERT INTO departments (department_id, department_name, manager_id, location_id)
Values(50,'Shipping',124,1500);
INSERT INTO departments (department_id, department_name, manager_id, location_id)
Values(60,'IT',103,1400);
INSERT INTO departments (department_id, department_name, manager_id, location_id)
Values(80,'Sales',149,2500);
INSERT INTO departments (department_id, department_name, manager_id, location_id)
Values(90,'Executive',100,1700);
INSERT INTO departments (department_id, department_name, manager_id, location_id)
Values(110,'Accounting',205,1700);
INSERT INTO departments (department_id, department_name, manager_id, location_id)
Values(190,'Contracting',null,1700);

# populate jobs table
INSERT INTO jobs (job_id, job_title, min_salary, max_salary)
Values('AD_PRES','President',20000,40000);
INSERT INTO jobs (job_id, job_title, min_salary, max_salary)
Values('AD_VP','Administration Vice President',15000,30000);
INSERT INTO jobs (job_id, job_title, min_salary, max_salary)
Values('AD_ASST','Administration Assistant',3000,6000);
INSERT INTO jobs (job_id, job_title, min_salary, max_salary)
Values('AC_MGR','Accounting Manager',8200,16000);
INSERT INTO jobs (job_id, job_title, min_salary, max_salary)
Values('AC_ACCOUNT','Public Accountant',4200,9000);
INSERT INTO jobs (job_id, job_title, min_salary, max_salary)
Values('SA_MAN','Sales Manager',10000,20000);
INSERT INTO jobs (job_id, job_title, min_salary, max_salary)
Values('SA_REP','Sales Representative',6000,12000);
INSERT INTO jobs (job_id, job_title, min_salary, max_salary)
Values('ST_MAN','Stock Manager',5500,8500);
INSERT INTO jobs (job_id, job_title, min_salary, max_salary)
Values('ST_CLERK','Stock Clerk',2000,5000);
INSERT INTO jobs (job_id, job_title, min_salary, max_salary)
Values('IT_PROG','Programmer',4000,10000);
INSERT INTO jobs (job_id, job_title, min_salary, max_salary)
Values('MK_MAN','Marketing Manager',9000,15000);
INSERT INTO jobs (job_id, job_title, min_salary, max_salary)
Values('MK_REP','Marketing Representative',4000,9000);

# populate employees table
INSERT INTO employees(employee_id,first_name,last_name,email,phone_INT,hire_date,job_id,salary,commission_pct,manager_id,department_id)
VALUES(100,'Steven','King','SKING','515.123.4567',  '1987-06-17' ,'AD_PRES',24000,null,null,90);
INSERT INTO employees(employee_id,first_name,last_name,email,phone_INT,hire_date,job_id,salary,commission_pct,manager_id,department_id)
VALUES(101,'Neena','Kochhar','NKOCHHAR','515.123.4568',  '1989-09-21' ,'AD_VP',17000,null,100,90 );
INSERT INTO employees(employee_id,first_name,last_name,email,phone_INT,hire_date,job_id,salary,commission_pct,manager_id,department_id)
VALUES(102,'Lex','De Haan','LDEHAAN','515.123.4569',  '1993-01-13' ,'AD_VP',17000,null,100,90 );
INSERT INTO employees(employee_id,first_name,last_name,email,phone_INT,hire_date,job_id,salary,commission_pct,manager_id,department_id)
VALUES(200,'Jennifer','Whalen','JWHALEN','515.123.4444',  '1987-09-17' ,'AD_ASST',4400,null,101,10 );
INSERT INTO employees(employee_id,first_name,last_name,email,phone_INT,hire_date,job_id,salary,commission_pct,manager_id,department_id)
VALUES(205,'Shelley','Higgins','SHIGGINS','515.123.8080',  '1994-06-07' ,'AC_MGR',12000,null,101,110 );
INSERT INTO employees(employee_id,first_name,last_name,email,phone_INT,hire_date,job_id,salary,commission_pct,manager_id,department_id)
VALUES(206,'William','Gietz','WGIETZ','515.123.8181',  '1994-06-07' ,'AC_ACCOUNT',8300,null,205,110 );
INSERT INTO employees(employee_id,first_name,last_name,email,phone_INT,hire_date,job_id,salary,commission_pct,manager_id,department_id, bonus)
VALUES(149,'Eleni','Zlotkey','EZLOTKEY','011.44.1344.429018',  '2000-01-29' ,'SA_MAN',10500,.2,100,80, '1500' );
INSERT INTO employees(employee_id,first_name,last_name,email,phone_INT,hire_date,job_id,salary,commission_pct,manager_id,department_id, bonus)
VALUES(174,'Ellen','Abel','EABEL','011.44.1644.429267',  '1996-05-11' ,'SA_REP',11000,.3,149,80,'1700' );
INSERT INTO employees(employee_id,first_name,last_name,email,phone_INT,hire_date,job_id,salary,commission_pct,manager_id,department_id, bonus)
VALUES(176,'Jonathon','Taylor','JTAYLOR','011.44.1644.429265',  '1998-03-24' ,'SA_REP',8600,.2,149,80,'1250' );
INSERT INTO employees(employee_id,first_name,last_name,email,phone_INT,hire_date,job_id,salary,commission_pct,manager_id,department_id)
VALUES(178,'Kimberely','Grant','KGRANT','011.44.1644.429263',  '1999-05-24' ,'SA_REP',7000,.15,149,null );
INSERT INTO employees(employee_id,first_name,last_name,email,phone_INT,hire_date,job_id,salary,commission_pct,manager_id,department_id)
VALUES(124,'Kevin','Mourgos','KMOURGOS','650.123.5234',  '1999-11-16' ,'ST_MAN',5800,null,100,50);
INSERT INTO employees(employee_id,first_name,last_name,email,phone_INT,hire_date,job_id,salary,commission_pct,manager_id,department_id)
VALUES(141,'Trenna','Rajs','TRAJS','650.121.8009',  '1995-10-17' ,'ST_CLERK',3500,null,124,50 );
INSERT INTO employees(employee_id,first_name,last_name,email,phone_INT,hire_date,job_id,salary,commission_pct,manager_id,department_id)
VALUES(142,'Curtis','Davies','CDAVIES','650.121.2994',  '1997-01-29' ,'ST_CLERK',3100,null,124,50 );
INSERT INTO employees(employee_id,first_name,last_name,email,phone_INT,hire_date,job_id,salary,commission_pct,manager_id,department_id)
VALUES(143,'Randall','Matos','RMATOS','650.121.2874',  '1998-03-15' ,'ST_CLERK',2600,null,124,50 );
INSERT INTO employees(employee_id,first_name,last_name,email,phone_INT,hire_date,job_id,salary,commission_pct,manager_id,department_id)
VALUES(144,'Peter','Vargas','PVARGAS','650.121.2004',  '1998-07-09' ,'ST_CLERK',2500,null,124,50 );
INSERT INTO employees(employee_id,first_name,last_name,email,phone_INT,hire_date,job_id,salary,commission_pct,manager_id,department_id)
VALUES(103,'Alexander','Hunold','AHUNOLD','590.423.4567',  '1990-01-03' ,'IT_PROG',9000,null,102,60 );
INSERT INTO employees(employee_id,first_name,last_name,email,phone_INT,hire_date,job_id,salary,commission_pct,manager_id,department_id)
VALUES(104,'Bruce','Ernst','BERNST','590.423.4568',  '1991-05-21' ,'IT_PROG',6000,null,103,60 );
INSERT INTO employees(employee_id,first_name,last_name,email,phone_INT,hire_date,job_id,salary,commission_pct,manager_id,department_id)
VALUES(107,'Diana','Lorentz','DLORENTZ','590.423.5567',  '1999-02-07' ,'IT_PROG',4200,null,103,60 );
INSERT INTO employees(employee_id,first_name,last_name,email,phone_INT,hire_date,job_id,salary,commission_pct,manager_id,department_id)
VALUES(201,'Michael','Hartstein','MHARTSTE','515.123.5555',  '1996-02-17' ,'MK_MAN',13000,null,100,20 );
INSERT INTO employees(employee_id,first_name,last_name,email,phone_INT,hire_date,job_id,salary,commission_pct,manager_id,department_id)
VALUES(202,'Pat','Fay','PFAY','603.123.6666',  '1997-08-17' ,'MK_REP',6000,null,201,20 );

# populate job_history table
INSERT INTO job_history(employee_id,start_date,end_date,job_id,department_id)
VALUES(200,'1987-09-17','1993-06-17','AD_ASST',90 );
INSERT INTO job_history(employee_id,start_date,end_date,job_id,department_id)
VALUES(101,'1993-10-28','1997-03-15','AC_MGR',110 );
INSERT INTO job_history(employee_id,start_date,end_date,job_id,department_id)
VALUES(200,'1994-07-01','1998-12-31','AC_ACCOUNT',90 );
INSERT INTO job_history(employee_id,start_date,end_date,job_id,department_id)
VALUES(101,'1989-09-21','1993-10-27','AC_ACCOUNT',110 );
INSERT INTO job_history(employee_id,start_date,end_date,job_id,department_id)
VALUES(176,'1999-01-01','1999-12-31','SA_MAN',80 );
INSERT INTO job_history(employee_id,start_date,end_date,job_id,department_id)
VALUES(176,'1998-03-24','1998-12-31','SA_REP',80 );
INSERT INTO job_history(employee_id,start_date,end_date,job_id,department_id)
VALUES(122,'1999-01-01','1999-12-31','ST_CLERK',50 );
INSERT INTO job_history(employee_id,start_date,end_date,job_id,department_id)
VALUES(114,'1998-03-24','1998-12-31','ST_CLERK',50 );
INSERT INTO job_history(employee_id,start_date,end_date,job_id,department_id)
VALUES(102,'1993-01-13','1998-07-24','IT_PROG',60 );
INSERT INTO job_history(employee_id,start_date,end_date,job_id,department_id)
VALUES(201,'1996-02-17','1999-12-19','MK_REP',20 );

# populate job_grades table
INSERT INTO job_grades(grade_level,lowest_sal,highest_sal)
VALUES('A',1000,2999);
INSERT INTO job_grades(grade_level,lowest_sal,highest_sal)
VALUES('B',3000,5999);
INSERT INTO job_grades(grade_level,lowest_sal,highest_sal)
VALUES('C',6000,9999);
INSERT INTO job_grades(grade_level,lowest_sal,highest_sal)
VALUES('D',10000,14999);
INSERT INTO job_grades(grade_level,lowest_sal,highest_sal)
VALUES('E',15000,24999);
INSERT INTO job_grades(grade_level,lowest_sal,highest_sal)
VALUES('F',25000,40000);


