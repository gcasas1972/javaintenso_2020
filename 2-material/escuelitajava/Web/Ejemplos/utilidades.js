function inicio() {
  alert("otro mensaje para Alejandra");
}
function imprimoEnHtml() {
  document.writeln("<h1>Bienvenidos a mi mundo de JS</h1>");
  document.writeln("Carita Feliz: <font size='+3'> \u263a");
  document.writeln('"Lindo dia. "');
}
function veoVariables() {
  var nombre = "Cristina";
  var apellido = "Perez";
  var edad = 28;
  var dni;
  var titulo = null;

  alert("edad: " + typeof edad); // nunber
  alert("nombre: " + typeof nombre); //string
  alert("titulo: " + typeof titulo); // object
  alert("dni: " + typeof dni); // undefined
}
function usoPrompt() {
  var nombre = prompt("Cual es su nombre? ", "");
  document.write("<br>Bienvenido !!!" + nombre + "<br>");
}
function usoConfirm() {
  if (confirm("Estas bien??") == true) {
    alert("Ok, avancemos");
  } else {
    alert("intentamos nuevamente");
  }
}
function usoOperadores() {
  var nro1 = 5;
  var nro2 = 7;
  var result = nro1 + nro2;
  document.writeln("la suma es: " + result + "<br>");
  result = nro1 - nro2;
  document.writeln("la resta es: " + result + "<br>");
  nro2 += 9; // nro2= nro2 + 9;
  document.writeln("la rpta de la ecuacion reducida es: " + nro2 + "<br>");
  nro2 %= 5;
  document.writeln("la rpta del resto es: " + nro2 + "<br>");
  // analizo post y pre incremento
  var x = 5;
  var y = 0;
  y = ++x; // add one to x first; then assign to y
  document.write("<h3>Pre-increment:<br>");
  document.write("y is " + y + "<br>");
  document.write("x is " + x + "<br>");
  document.write("-----------------------<br>");
  var x = 5;
  var y = 0;
  y = x++; // assign value in x to y; then add one to x
  document.write("<h3>Post-increment:<br>");
  document.write("y is " + y + "<br>");
  document.write("x is " + x + "<br></h3>");
}
function usoOpeLogic() {
  var num1 = 50;
  var num2 = 100;
  var num3 = 0;
  document.write("<h3>num1 && num2 is " + (num1 && num2) + ".<br>");
  document.write("num1 || $num2 is " + (num1 || num2) + ".<br>");
  document.write("! num1 is " + !num1 + ".<br>");
  document.write("!(num1 && num2) is " + !(num1 && num2) + ".<br>");
  document.write("!(num1 && num3) is " + !(num1 && num3) + ".<br>");

  var answer = prompt("Donde te gustaria almorzar? ", "");
  if (answer == "McDonald's" || answer == "Taco Bell" || answer == "Wendy's") {
    alert("No fast food hoy, gracias.");
  } else {
    alert("Genial, comida Gourmet.");
  }

  answer = prompt("Cuantos años tienes? ", "");
  if (answer > 12 && answer < 20) {
    alert("Teenagers rock!");
  } else {
    alert("superaste la adolescencia o no ...");
  }
}
function usoOpeDesplazamiento() {
  var result = 15 & 9;
  document.write("15 & 9 cm: " + result);
  result = 15 | 9;
  document.write("<br> 15 | 9 cm: " + result);
  result = 15 ^ 9;
  document.write("<br> 15 ^ 9 cm: " + result);
  result = 9 << 2;
  document.write("<br> 9 << 2 cm: " + result);
  result = 9 >> 2;
  document.write("<br> 9 >> 2 cm: " + result);
  result = -9 >> 2;
  document.write("<br> -9 >> 2 cm: " + result);
  result = 15 >>> 2;
  document.write("<br> 15 >>> 2 cm: " + result);
}
function convertir() {
  var temp = prompt("Cual es su temperatura? ", "");
  alert("temp es: " + typeof temp); //string ... 37 .... "37"
  temp = parseFloat(temp); // "37" ... 37.0
  if (temp == 36.6) {
    alert("Es normal");
  } else {
    alert("Tenes covid!");
  }
}
function usoSelectorCasos() {
  var color = prompt("Cual es su color favorito?", "");
  switch (color) {
    case "red":
      document.body.style.backgroundColor = "red";
      break;
    case "yellow":
      document.body.style.backgroundColor = color;
      break;
    case "green":
      document.body.style.backgroundColor = color;
      break;
    case "blue":
      document.body.style.backgroundColor = color;
      break;
    default:
      document.body.style.backgroundColor = color;
      break;
  }
}
function usoBucles() {
  document.write("<font size='+2'>");
  var i = 0;
  // la ejecucion de 0 a muchos
  while (i < 10) {
    // Test
    document.writeln(i);
    i++; // Increment the counter
  }
  document.writeln("<br>");
  // la ejecucion de 1 a muchos
  document.write("<font size='+2'>");
  var i = 0;
  do {
    document.writeln(i);
    i++;
  } while (i < 10);
}
function usoFor() {
  document.write("<font size='+2'>");
  for (var i = 0; i < 10; i++) {
    document.writeln(i);
  }
  document.write("<br>");
  // anidamiento
  var str = "@";
  for (var row = 0; row < 6; row++) {
    for (var col = 0; col < row; col++) {
      document.write(str);
    }
    document.write("<br>");
  }
}
function veoVectores() {
  let dias = [
    "Lunes",
    "Martes",
    "Miércoles",
    "Jueves",
    "Viernes",
    "Sábado",
    "Domingo",
  ];

  for (var i = 0; i < dias.length; i++) {
    document.writeln(i + " --> " + dias[i]);
  }
  document.write("<br>");

  dias.forEach((dia) => document.writeln(dia)); // version lambdas

  document.write("<br>");
  for (const dia of dias) {
    // OF: muestra el valor del elemento
    document.writeln(dia);
  }
  document.write("<br>");
  for (const dia in dias) {
    // IN: muestra la posicion del elemento
    document.writeln(dia);
  }
}
function defString() {
  var frase = "Vientos de guerra estan soplando ....Putinnnnnnn";
  var frase2 = new String("Hay paz en Argentina.... digamos....");
  document.write("la 1era frase es de tipo: " + typeof frase);
  document.write("<br>");
  document.write("la 2da frase es de tipo: " + typeof frase2);
}
function cantCaract() {
  var frase = "Vientos de guerra estan soplando ....Putinnnnnnn";
  document.write("la 1era frase tiene: " + frase.length);
}
function pasarAmayus() {
  var frase2 = new String("Hay paz en Argentina.... digamos....");
  document.write(frase2.toUpperCase());
  document.write("<br>");
  document.write(frase2.toLowerCase());
}
function pasarAminus(frase) {
  return frase.toLowerCase();
}

function cambioLetras() {
  var frase2 = new String("Hay paz en Argentina.... digamos....");
  document.write(pasarAminus(frase2));
  document.write("<br>");
  var fraseConvertida = pasarAminus(frase2);
  document.write(fraseConvertida);
  document.write("<br>");
  pasarAminus(frase2); // si bien no da error, no se recibe en nigun lado
}

function cambioColoresyFonts() {
  var frase = new String("hola mundo!!!");
  var frase1 = "chauu mundo!!!";
  document.write(frase.fontcolor("blue"));
  document.write("<br>");
  document.write(frase.fontsize(24));
  document.write("<br>");
  document.write(frase.fontsize(24).fontcolor("red"));
  document.write("<br>");
  document.write(frase1.fontsize(24).fontcolor("orange"));
  document.write("<br>");
}
function buscoIndice() {
  // indexOf
  var email = "alegmail.com";
  while (email.indexOf("@") == -1) {
    alert("email invalido");
    break;
  }
  document.write("<br> OK ");
}

function veoSubstrings() {
  // substring
  var frase = "Hay paz en Argentina.... digamos....";
  document.write(frase.substring(11, 20)); // posicion inicio, posicion fin ..substr
  document.write("<br>");
}
function desdoblar() {
  // split y el chartAt
  var email = "ale@gmail.com@micorreo";
  var parte1 = email.split("@");
  document.write(parte1[0]);
  document.write("<br>");
  document.write(parte1[1]);
  document.write("<br>");
  document.write(parte1[2]);
  document.write("<br>");

  document.write("el caracter en la posicion 4 es " + email.charAt(3));
}
function buscoYreemplazo() {
  var email = "DanielSavage@dadserver.org";
  var nuevoemail = email.replace("Daniel", "Juan");
  document.write(email);
  document.write("<br>");
  document.write(nuevoemail);
}

function jugandoConMath() {
  var num = 16;
  document.write("<h3>The square root of " + num + " is ");
  document.write(Math.sqrt(num), ".<br>");
  document.write("PI is ");
  document.write(Math.PI);
  document.write(".<br>" + num + " raised to the 3rd power is ");
  document.write(Math.pow(num, 3));
  document.write(".</h3></font>");
  num = 16.3;
  document.write("<I>The number being manipulated is: ", num, "</I><br><br>");
  document.write(
    "The <I>Math.floor</I> method rounds down: " + Math.floor(num) + "<br>" );
  document.write(
    "The <I>Math.ceil</I> method rounds up: " + Math.ceil(num) + "<br>" );
  document.write(
    "The <I>Math.round</I> method rounds to the nearest integer: " + Math.round(num) +"<br>" );

  var n = 10;
  for (i = 0; i < 10; i++) {
    // Generate random numbers between 0 and 10
    document.write(Math.floor(Math.random() * (n + 1)) + "<br>");
  }
}
