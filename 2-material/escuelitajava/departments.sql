CREATE TABLE   DEPARTMENTS 
   ( DEPARTMENT_ID  INT,
     DEPARTMENT_NAME  VARCHAR(30) NOT NULL  ,
     MANAGER_ID  INT,
     LOCATION_ID  INT,
     CONSTRAINT  DEPT_ID_PK  PRIMARY KEY ( DEPARTMENT_ID )
     
   );

# populate departments table
INSERT INTO departments (department_id, department_name, manager_id, location_id)
Values(10,'Administration',200,1700);
INSERT INTO departments (department_id, department_name, manager_id, location_id)
Values(20,'Marketing',201,1800);
INSERT INTO departments (department_id, department_name, manager_id, location_id)
Values(50,'Shipping',124,1500);
INSERT INTO departments (department_id, department_name, manager_id, location_id)
Values(60,'IT',103,1400);
INSERT INTO departments (department_id, department_name, manager_id, location_id)
Values(80,'Sales',149,2500);
INSERT INTO departments (department_id, department_name, manager_id, location_id)
Values(90,'Executive',100,1700);
INSERT INTO departments (department_id, department_name, manager_id, location_id)
Values(110,'Accounting',205,1700);
INSERT INTO departments (department_id, department_name, manager_id, location_id)
Values(190,'Contracting',null,1700);
